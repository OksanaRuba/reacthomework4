import React, { useState, useEffect } from 'react';

import Header from '../header';
import RandomPlanet from '../random-planet';
import ItemList from '../item-list';
import PersonDetails from '../person-details';
import Service from '../../services/service';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";


import './app.css';

const App = () => {
  const dataService = new Service();
 /*const [test, setTest] = useState(null);
  useEffect(() => {
    new Service().getStarships()
      .then((data) => { setTest(data.results) })
      .catch((massege) => { console.error(massege) })

  }, []);
  */
  return (
    <Router>
      <Header></Header>
      <RandomPlanet />
      <div className='row mb2'>
      <Switch>
        <Route exact path='/people'>
          <div className='col-md-6'>
            <ItemList getData={dataService.getPeoples.bind(dataService)} key='1'/>
          </div>
        </Route>
        <Route  path='/planets'>
        <div className='col-md-6'>
            <ItemList getData={dataService.getPlanets.bind(dataService)} key='2'/>
          </div>
        </Route>
        <Route path='/starships' >
        <div className='col-md-6'>
            <ItemList getData={dataService.getStarships.bind(dataService)} key='3'/>
          </div>
        </Route> 
      </Switch>
        <div className='col-md-6'>
          <PersonDetails />
        </div>
      </div>
    </Router>
  );
};

export default App;