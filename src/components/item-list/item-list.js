import React, { useState, useEffect } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

import './item-list.css';

const ItemList = ({ getData }) => {
  const [data, setData] = useState([])
  useEffect(() => {
    async function loadData() {
      const { results } = await getData();
      setData(results) //зберігаємо нові дані
    }
    if (!data.length) {
      loadData();
    }
  }, [getData])
  return (
    <ul className="item-list list-group">
      {data === null ? <CircularProgress /> : data.map((item, index) => {
        return <li className="list-group-item" key={index * 3 + 'r'}> {item.name} </li>
      })}
    </ul>
  );

}

export default ItemList;
